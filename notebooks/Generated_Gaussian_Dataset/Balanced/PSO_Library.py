#!/usr/bin/env python
# coding: utf-8

# In[ ]:

from sklearn import svm
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
import math

def Sphere(x):
    sum = 0
    for i in range(0,len(x)):
        sum = sum + (x[i])**2
    
    return sum

def Matrix(nrows, ncols):
    matrix = []
    for i in range(0,nrows):
        matrix.append([])
        for j in range(0,ncols):
            matrix[i].append(5)
    return matrix


def DataMean(data):
    b = []
    for i in range(0,data.shape[1]-1):
        b.append(0)
    
    for j in range(0,data.shape[1]-1):
        sum = 0
        for i in range(0,data.shape[0]):
            sum = sum + data.iat[i,j]
        b[j] = sum/data.shape[0]
        
    return b


def ClassMean(data,k):
    b = []
    for i in range(0,data.shape[1]-1):
        b.append(0)
        
    for j in range(0,data.shape[1]-1):
        sum = 0 
        for i in range(0,data[data[data.columns[data.shape[1]-1]] == k].shape[0]):
            sum = sum + data[data[data.columns[data.shape[1]-1]] == k].iat[i,j]
        b[j] = sum/data[data[data.columns[data.shape[1]-1]] == k].shape[0]
        
    return b


def InClassScatterPolyKernel(x, data):
    
    data = data.copy(deep=True)
    
    data["phi(X)"] = data.apply(lambda row: (row.X + x[1])**(math.floor(x[0])), axis = 1)
    data = data[["X", "phi(X)", "Class"]]
    
    distinct_classes = np.unique(data.iloc[:,2])
    a = 0
    
    for k in distinct_classes:
        b = 0
        for l in range(0,data.shape[0]):
            if (data.iloc[:,2][l] == k):
                b = b + np.dot(data.iloc[l,0:2] - ClassMean(data, k), data.iloc[l,0:2] - ClassMean(data, k))
        a = a + b
    
    return a


def InClassScatterRBFKernel(x, data):
    
    data = data.copy(deep=True)
    
    data['phi(X)'] = data.apply(lambda row: math.exp(-x[0]*(row.X)**2), axis = 1)
    data = data[["X", "phi(X)", "Class"]]
    
    distinct_classes = np.unique(data.iloc[:,2])
    a = 0
    
    for k in distinct_classes:
        b = 0
        for l in range(0,data.shape[0]):
            if (data.iloc[:,2][l] == k):
                b = b + np.dot(data.iloc[l,0:2] - ClassMean(data, k), data.iloc[l,0:2] - ClassMean(data, k))
        a = a + b
    
    return a
    


def BetweenClassScatterPolyKernel(x, data):
    
    data = data.copy(deep=True)
    
    data['phi(X)'] = data.apply(lambda row: (row.X + x[1])**(math.floor(x[0])), axis = 1)
    data = data[["X", "phi(X)", "Class"]]
    
    distinct_classes = np.unique(data.iloc[:,2])
    a = 0
    
    for k in distinct_classes:
        a = a + (data[data[data.columns[data.shape[1]-1]] == k].shape[0])*np.dot(np.subtract(ClassMean(data, k), DataMean(data)),
                                                                                 np.subtract(ClassMean(data, k), DataMean(data)))
    
    return a 

def BetweenClassScatterRBFKernel(x, data):
    
    data = data.copy(deep=True)
    
    data['phi(X)'] = data.apply(lambda row: math.exp(-x[0]*(row.X)**2), axis = 1)
    data = data[["X", "phi(X)", "Class"]]
    
    distinct_classes = np.unique(data.iloc[:,2])
    a = 0
    
    for k in distinct_classes:
        a = a + (data[data[data.columns[data.shape[1]-1]] == k].shape[0])*np.dot(np.subtract(ClassMean(data, k), DataMean(data)),
                                                                                 np.subtract(ClassMean(data, k), DataMean(data)))
    
    return a 


def AccuracyPolyKernel(x, data_X, data_Y):
    
    df = pd.DataFrame(data_X)
    target = pd.DataFrame(data_Y)
    
    model = svm.SVC(kernel = 'poly',degree = x[0], coef0 = x[1])
    model.fit(df, target)
    
    guess = model.predict(df)
    
    return accuracy_score(data_Y, guess)


def AccuracyRBFKernel(x, data_X, data_Y):
    
    df = pd.DataFrame(data_X)
    target = pd.DataFrame(data_Y)
    
    model = svm.SVC(kernel = 'rbf', gamma = x[0])
    model.fit(df, target)
    
    guess = model.predict(df)
    
    return accuracy_score(data_Y, guess)
    
    
####################### Extra functions (the ones not needed) #############################################
    
    
def ExtraFunction1(x, data_X, data_Y):
    
    df = pd.DataFrame(data_X)
    target = pd.DataFrame(data_Y)
    
    model = svm.SVC(kernel = 'poly',degree = x[0], coef0 = x[1])
    model.fit(df, target)
    
    guess = model.predict(df)
    
    b = 0
    distinct_classes = np.unique(guess)
    data = pd.concat([df,pd.DataFrame(guess)], axis=1, ignore_index = True)
    a = 0
    
    for k in distinct_classes:
        b = 0
        for l in range(0,data_X.shape[0]):
            if (data_Y[l] == k):
                b = b + (data_X[l] - ClassMean(data, k))**2
        a = a + b
    
    return a


def ExtraFunction2(x, data_X, data_Y):
    
    df = pd.DataFrame(data_X)
    target = pd.DataFrame(data_Y)
    
    model = svm.SVC(kernel = 'poly',C = x[0])
    model.fit(df, target)
    
    guess = model.predict(df)
    
    b = 0
    distinct_classes = np.unique(guess)
    data = pd.concat([df,pd.DataFrame(guess)], axis=1, ignore_index = True)
    a = 0
    
    for k in distinct_classes:
        b = 0
        for l in range(0,data_X.shape[0]):
            if (data_Y[l] == k):
                b = b + (data_X[l] - ClassMean(data, k))**2
        a = a + b
    
    return a


def ExtraFunction3(x, data_X, data_Y):
    
    df = pd.DataFrame(data_X)
    target = pd.DataFrame(data_Y)
    
    model = svm.SVC(kernel = 'poly',C = x[0])
    model.fit(df, target)
    
    guess = model.predict(df)
    
    return - accuracy_score(data_Y, guess)