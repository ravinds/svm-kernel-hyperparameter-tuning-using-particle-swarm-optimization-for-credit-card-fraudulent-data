# SVM on Imbalanced Credit Card Data and Kernel Hyperparameter Tuning through Particle Swarm Optimization (PSO)

• Developed a Python code for conducting comparative study on different optimization algorithms and its objective functions on balanced/imbalanced credit card fraudulency data in Kernel Hyperparameter Tuning process of SVM algorithm.


• Used SMOTE on unbalanced data and devised new optimization’s objectives; Between-Class Scatter and In-Class Scatter.


• PSO algorithm achieved 3.5-16.67% better accuracy than Exhaustive Grid Search and Randomized Parameter Optimization.


• Inferenced powerful insights like minimizing the In-Class Scatter or maximizing the Between-Class Scatter simultaneously maximized the Accuracy, and it is easier to linearly separate imbalanced data than balanced data in feature space.

***Techniques Used:*** PSO, SVM, Scikit-learn, SMOTE, GridSearchCV, RandomizedSearchCV. (**Python**)
